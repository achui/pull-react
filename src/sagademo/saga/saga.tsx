import {watchIncrementAsync} from "./saga-counter"
import {watchAjax} from "./saga-ajax"
import { put, fork } from 'redux-saga/effects'
export function* rootSaga() {
    yield [
        fork(watchIncrementAsync),
        fork(watchAjax)
    ]
}
