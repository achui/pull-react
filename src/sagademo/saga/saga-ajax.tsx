import { takeEvery } from 'redux-saga'
import { put, call } from 'redux-saga/effects'
import cFetch from '../../mydemo/service/baseAjaxService'
export function* helloSaga() {
    console.log("Hello Saga");
}

// 一个工具函数：返回一个 Promise，这个 Promise 将在 1 秒后 resolve
export const delay = ms => new Promise(resolve => setTimeout(resolve, ms))

// Our worker Saga: 将异步执行 increment 任务

export function fetchData() {
    return cFetch("https://cors-anywhere.herokuapp.com/https://www.reddit.com/r/front.json" , "GET", {})
}
export function* executeAjax() {
  yield put({type: "AJAX_REQUEST_START"})
  try {
      const data = yield call(fetchData)
      //const data = yield cFetch("https://cors-anywhere.herokuapp.com/https://www.reddit.com/r/front.json","GET",{})
      yield put({type:"AJAX_REQUEST_SUCCESS", response:data})
  } catch (error) {
      yield put({ type: 'AJAX_REQUEST_FAIL', error : error.message || "Error happen" })
  }


}

// Our watcher Saga: 在每个 INCREMENT_ASYNC action 调用后，派生一个新的 incrementAsync 任务
export function* watchAjax() {
  yield* takeEvery('AJAX_ASYNC', executeAjax)
}
