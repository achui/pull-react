import {incrementAsync} from "../sagademo/saga/saga-counter";
import { put, call } from 'redux-saga/effects'

describe("saga counter test", () => {
    const gen = incrementAsync();

    it("result 1", () => {
        let result = gen.next();
        result = gen.next();
        expect(result.value).toEqual(put({ type: 'INCREASE_COUNTER', playLoad : 1 }));
    })
} )
