import * as React from 'react';
import * as renderer from 'react-test-renderer';
import { shallow, mount, ShallowWrapper, ReactWrapper } from "enzyme"
import configureStore from 'redux-mock-store'
import { Provider } from 'react-redux'
import { Hello } from '../components/Hello'
import { Counter } from '../mydemo/container/Counter'
import { CounterComponent } from '../mydemo/componment/Counter'
import { createStore, combineReducers } from 'redux'
import reducers from "../mydemo/reducers/CounterRecucer"

/*describe(">>>>> Hello for snapshot", () => {
    it("capture snapshot for counter", () => {
        const renderValue = renderer.create(<Hello compiler="Typescript" framework="React" />).toJSON();
        expect(renderValue).toMatchSnapshot();
    })
})*/

describe(">>>>> Counter ", () => {
    const initialState = { counter: { value: 0 } }
    const mockStore = configureStore()
    let store, container;
    let wrapper: ShallowWrapper<any, any>;

    beforeEach(() => {
        store = mockStore(initialState);
        wrapper = shallow(<Counter store={store} label="bbb" />)
    })

    it("render label", () => {
        expect(wrapper.prop("label")).toEqual("bbb");
    })

    it("render counter", () => {
        const renderValue = renderer.create(<Counter store={store} label="bbb" />).toJSON();
        expect(renderValue).toMatchSnapshot();
    })
})

describe(">>> Counter page with provder and store", () => {
    const initialState = { counter: { value: 0 } }
    const mockStore = configureStore();
    let store;
    let wrapper: ReactWrapper<any, any>;
    let onIncrease = jest.fn()
    const props = {
        increase: jest.fn()
    }
    beforeEach(() => {

        store = createStore(combineReducers({
            ...reducers
        }));
        //store = mockStore(initialState);
        wrapper = mount(<Provider store={store}><Counter label="ccc"/></Provider>)
    })

    it('+++ contains header - label', () => {
        expect(wrapper.contains(<b>label: ccc</b>)).toBe(true)
    });

    it('+++ contains header - button', () => {
        expect(wrapper.find("#abc").text()).toBe("+++")
    });

    it("test increase", () => {
        // const renderValue = renderer.create(<Counter store={store} label="aa" />);
        // let tree = renderValue.toJSON();
        //expect(wrapper).toMatchSnapshot();

        // store.dispatch({
        //     type:'INCREASE_COUNTER',
        //     playLoad:3
        // })
        const button = wrapper.find("button").first();
        console.dir(button)
        button.simulate("click");
        //expect(onIncrease.mock.calls[0][0]).toBe('1');
        expect(wrapper.contains(<p># Counter : 1</p>)).toBe(true);
    })
})

