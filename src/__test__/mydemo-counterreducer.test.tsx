import reducers from "../mydemo/reducers/CounterRecucer"

describe("Reducer ----> test counter reducer", () => {
    it("test increase", () => {
        let state = {value:100}
        state = reducers.counter(state, {
            type:"INCREASE_COUNTER",
            playLoad:100
        });
        expect(state).toEqual({value:200})
    })
})
