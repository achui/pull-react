import { increaseCounterAsync, increaseCounter } from "../mydemo/actions/Action"

describe("Action----> Test increaseCounter", () => {
    it("test increate", () => {
        const increase = increaseCounter(1);
        expect(increase).toEqual({
            type: "INCREASE_COUNTER",
            playLoad: 1
        })
    })

    it("test mock async", () => {
        const increase = increaseCounterAsync();
        expect(increase).toEqual({
            type: "INCREMENT_ASYNC"
        })
    })
})
