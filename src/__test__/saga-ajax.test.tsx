import { executeAjax, fetchData } from "../sagademo/saga/saga-ajax";
import { put, call } from 'redux-saga/effects'
import cFetch from '../mydemo/service/baseAjaxService'
describe("saga ajax test", () => {
    const gen = executeAjax();

    it("result put ajax start", () => {
        let result = gen.next();
        expect(result.value).toEqual(put({ type: "AJAX_REQUEST_START" }));
    })

    it("result put ajax success", () => {
        let result = gen.next();
        expect(result.value).toEqual(call(fetchData));
    })


    it("result put ajax error", () => {
        let result = gen.throw({message:"Error while ajax"})
        expect(result.value).toEqual(put({ type: 'AJAX_REQUEST_FAIL', error: "Error while ajax" }));
    })



})
