import * as React from 'react'

export default class Posts extends React.Component<any,any> {
  render() {
    return (
      <ul>
        {this.props.posts.map((post:any, i:any) =>
          <li key={i}>{post.title}</li>
        )}
      </ul>
    )
  }
}