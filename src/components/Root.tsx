import * as React from 'react'
import { Provider } from 'react-redux'
import configureStore from '../store/NewStore'
import AsyncApp from './AsyncApp'

const store = configureStore({});

export default class Root extends React.Component<any, any> {
    render() {
        return (
            <Provider store={store}>
                <AsyncApp />
            </Provider>
        )
    }
}
