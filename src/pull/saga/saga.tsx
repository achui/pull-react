import { watchAjax } from "./saga-ajax"
import { fork } from 'redux-saga/effects'
export function* rootSaga() {
    yield [
        fork(watchAjax)
    ]
}
