import * as React from 'react'
import * as ReactDOM from "react-dom";
import {RootComp} from './container/Root'


ReactDOM.render(
  <RootComp />,
  document.getElementById('example')
)
