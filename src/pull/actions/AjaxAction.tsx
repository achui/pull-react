import { RequestOption } from "../service/baseAjaxService"
export const AJAX_CALL_START = "AJAX_CALL_START";
export const AJAX_CALL_SUCCESS = "AJAX_CALL_SUCCESS";
export const AJAX_CALL_FAIL = "AJAX_CALL_FAIL";

export function ajaxCall(url, method, params) {
    return {
        type: AJAX_CALL_START,
        playLoad: { url, method, params },
        isLoading: true
    }
}
export function ajaxBatchCall(requests: RequestOption[]) {
        return {
        type: AJAX_CALL_START,
        playLoad: { requests },
        isLoading: true
    }
}
export function ajaxCallSuccess(data) {
    return {
        type: AJAX_CALL_SUCCESS,
        playLoad: { data },
        isLoading: false
    }
}

export function ajaxCallFail(error) {
    return {
        type: AJAX_CALL_SUCCESS,
        playLoad: { error },
        isLoading: false
    }
}
