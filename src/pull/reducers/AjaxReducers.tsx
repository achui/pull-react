import { combineReducers } from 'redux'
import { AJAX_CALL_START, AJAX_CALL_SUCCESS, AJAX_CALL_FAIL } from "../actions/AjaxAction"

export function ajaxReducer(state = {}, action) {
    switch (action.type) {
        case AJAX_CALL_START:
            return {
                ...state, isLoading: true, playLoad: action.playLoad
            };
        case AJAX_CALL_SUCCESS:
            return { ...state, isLoading: false, data: action.data }
        case AJAX_CALL_FAIL:
            return { ...state, isLoading: false };
        default:
            return state;
    }
}
export default { ajaxReducer }
