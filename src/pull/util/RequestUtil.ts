import { RequestOption } from './../service/baseAjaxService';
import { ProjInfo } from './../config/ReleaseComp';
import * as Crypto from "crypto-js";
export class RequestUtil {
    static basicAuth(username: string, pwd: string): string {
        let basic = username.concat(":").concat(pwd);
        var wordArray = Crypto.enc.Utf8.parse(basic);
        var base64 = Crypto.enc.Base64.stringify(wordArray);
        console.log("base64:" + base64);
        return "Basic ".concat(base64);
    }

    static buildCreateBranchRequest(featureBranch: ProjInfo, username: string, pwd: string, branchName: string, issueNo: string): any {
        const {projectKey, slug} = featureBranch;
        let ajaxUrl = `/rest/branch-utils/latest/projects/${projectKey}/repos/${slug}/branches`;
        let basic = RequestUtil.basicAuth(username, pwd);
        let headers = {
            "Authorization": basic
        }
        let request:RequestOption = {
            url: ajaxUrl,
            option: {
                headers: headers,
                body: JSON.stringify({
                    "name": "feature/" + issueNo,
                    "startPoint": "refs/heads/" + branchName
                })
            },
            method: "POST"

        };
        return request;

    }
}
