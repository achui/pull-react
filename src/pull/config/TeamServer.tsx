export interface TeamServerConfig {
   comp:string
   yml?:string
   create?:boolean
   issueNum?:string
   issueNumWithNoAbbr?:string
}
export const TeamServerList: TeamServerConfig[] = [
{
    comp:"BOV1-BOSEV1", yml:"bov1-bosev1", create:true, issueNum:"", issueNumWithNoAbbr:""
},
{
    comp:"BOV2-Backend", yml:"bov2-backend", create:true, issueNum:"", issueNumWithNoAbbr:""
},
{
    comp:"BOSEV2-Backend", yml:"bosev2-backend", create:false, issueNum:"", issueNumWithNoAbbr:""
},
{
    comp:"PCI-Process-Manager", yml:"bopcipm", create:false, issueNum:"", issueNumWithNoAbbr:""
},
{
    comp:"PCI-Web-Service", yml:"bopciws", create:false, issueNum:"", issueNumWithNoAbbr:""
},
{
    comp:"Web-Service", yml:"bows", create:false, issueNum:"", issueNumWithNoAbbr:""
},
{
    comp:"PCI-Process-Manager-Singleton", yml:"bopcipm-singleton", create:false, issueNum:"", issueNumWithNoAbbr:""
},
{
    comp:"PCI-Web-Service-Singleton", yml:"bopciws-singleton", create:false, issueNum:"", issueNumWithNoAbbr:""
},
{
    comp:"Frontend", yml:"frontend", create:true, issueNum:"", issueNumWithNoAbbr:""
},
]

export function getTeamServerByCheckValue(checkValue:string[]):TeamServerConfig[] {
    let tsList = TeamServerList;
    return tsList.filter((item:TeamServerConfig) => {
        let val = checkValue.find((itm:string) => {
            return item.comp === itm;
        })
        return val !== undefined;
    })
}

export function getDispayTeamServer():string[] {
    let tsList = TeamServerList;
    return tsList.map((item:TeamServerConfig) => {
        return item.comp;
    });
}
