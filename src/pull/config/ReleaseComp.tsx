export interface ProjInfo {
    componment:string
    category?:string
    projectKey?:string
    willRelease?:boolean
    slug?:string
    desc?:string
}
export const ReleaseComponment: ProjInfo[] = [
    { componment: "iStoreV2-Backend", category: "ISTORENH", projectKey: "ISTORENH", willRelease: false, slug: "istorenh_backend", desc: "" },
    { componment: "iStoreV2-Frontend", category: "ISTORENH", projectKey: "ISTORENH", willRelease: false, slug: "istorenh_frontend", desc: "" },
    { componment: "iStoreV1", category: "PARTNER", projectKey: "PARTNER", willRelease: false, slug: "castro", desc: "" },
    { componment: "PCI-AppTransfer", category: "PCI", projectKey: "PCI", willRelease: false, slug: "applicationtransfer", desc: "" },
    { componment: "PCI-Boservice", category: "PCI", projectKey: "PCI", willRelease: false, slug: "boservice", desc: "" },
    { componment: "PCI-EPI3", category: "PCI", projectKey: "PCI", willRelease: false, slug: "epi3", desc: "" },
    { componment: "PCI-ESB", category: "PCI", projectKey: "PCI", willRelease: false, slug: "esb", desc: "" },
    { componment: "PCI-InternalService", category: "PCI", projectKey: "PCI", willRelease: false, slug: "internalservice", desc: "" },
    { componment: "PCI-PCIService", category: "PCI", projectKey: "PCI", willRelease: false, slug: "pciservice", desc: "" },
    { componment: "PCI-PCIServiceV2", category: "PCI", projectKey: "PCI", willRelease: false, slug: "pciservicev2", desc: "" },
    { componment: "WC-BOServiceV2", category: "WEB", projectKey: "WEB", willRelease: false, slug: "boservicev2", desc: "" },
    { componment: "WC-FOService", category: "WEB", projectKey: "WEB", willRelease: false, slug: "foservice", desc: "" },
    { componment: "WC-FOServiceV2", category: "WEB", projectKey: "WEB", willRelease: false, slug: "foservicev2", desc: "" },
    { componment: "WC-InternalServiceV2", category: "WEB", projectKey: "WEB", willRelease: false, slug: "internalservicev2", desc: "" },
    { componment: "WC-Report", category: "WEB", projectKey: "WEB", willRelease: false, slug: "Report", desc: "" },
    { componment: "WC-StatusUpdate", category: "WEB", projectKey: "WEB", willRelease: false, slug: "statusupdate", desc: "" },
    { componment: "WC-webconnect_conf", category: "WEB", projectKey: "WEB", willRelease: false, slug: "webconnect_conf", desc: "" },
    { componment: "Backend-Demo", category: "ISTORENH", projectKey: "ISTORENH", willRelease: false, slug: "istorenh_demo_backend", desc: "" },
    { componment: "Frontend-Demo", category: "ISTORENH", projectKey: "ISTORENH", willRelease: false, slug: "istorenh_demo_frontend", desc: "" },
    { componment: "bov1", category: "BO", projectKey: "BO", willRelease: false, slug: "castro", desc: "" },
    { componment: "bov2Backend", category: "BO", projectKey: "BO", willRelease: false, slug: "bov2-backend", desc: "" },
    { componment: "bov2Frontend", category: "BO", projectKey: "BO", willRelease: false, slug: "bov2-frontend", desc: "" },
    { componment: "bopcipm", category: "BO", projectKey: "BO", willRelease: false, slug: "pci-process-manager", desc: "" },
    { componment: "bopciws", category: "BO", projectKey: "BO", willRelease: false, slug: "pci-web-service", desc: "" },
    { componment: "bows", category: "BO", projectKey: "BO", willRelease: false, slug: "web-service", desc: "" },
    { componment: "boservicev2", category: "BO", projectKey: "WEB", willRelease: false, slug: "boservicev2", desc: "" },
    { componment: "daemonservice", category: "BO", projectKey: "PCI", willRelease: false, slug: "daemonservice", desc: "" },
    { componment: "bosev2Content", category: "BO", projectKey: "BOSE", willRelease: false, slug: "content", desc: "" },
    { componment: "bosev2Backend", category: "BO", projectKey: "BOSE", willRelease: false, slug: "backend", desc: "" },
    { componment: "bosev2Frontend", category: "BO", projectKey: "BOSE", willRelease: false, slug: "frontend", desc: "" },
    { componment: "foservice", category: "BO", projectKey: "WEB", willRelease: false, slug: "foservice", desc: "" },
    { componment: "fast-pi", category: "BO", projectKey: "BO", willRelease: false, slug: "fast-pi", desc: "" }]

export function getReleaseCompsByCategory(key:string):ProjInfo[] {
    let releaseComps = ReleaseComponment;
    return releaseComps.filter((item:ProjInfo) => {
        return item.category === key;
    })
}

export function getDisplayCompsByCategory(key:string):string[] {
     let releaseComps = ReleaseComponment;
     return releaseComps.filter((item:ProjInfo) => {
        return item.category === key;
    }).map((item:ProjInfo) => {
        return item.componment
    })
}

export function getReleaseCompsByCheckValues(checkValues:string[]):ProjInfo[] {
    let releaseComps = ReleaseComponment;
    return releaseComps.filter((item:ProjInfo) => {
        let val = checkValues.find((itm:string) => {
            return itm === item.componment
        })
        return val !== undefined;
    })
}

export function getReleaseCompsByComponment(componment:string):ProjInfo {
    return ReleaseComponment.find((item:ProjInfo) => {
        return item.componment === componment
    })
}
