import * as React from 'react';
import { Provider } from 'react-redux';
import { createStore, combineReducers, applyMiddleware } from "redux";
import { createLogger } from 'redux-logger';
import createSagaMiddleware from 'redux-saga';
import { ajaxReducer } from "../reducers/AjaxReducers"
import { rootSaga } from "../saga/saga"
//import { rootSaga } from '../../sagademo/saga/saga'
import { App } from "../container/App"
const sagaMiddleware = createSagaMiddleware()
const loggerMiddleware = createLogger();
const store = createStore(
    combineReducers({
        ajaxReducer
    }),
    applyMiddleware(sagaMiddleware,loggerMiddleware)
)
sagaMiddleware.run(rootSaga)
export class RootComp extends React.Component<any, any> {
    render() {
        return (
            <Provider store={store}>
                <App />
            </Provider>
        );
    }
}
