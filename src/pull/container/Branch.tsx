import * as React from "react"
import { Form, Input, Button, Checkbox, Row, Col } from "antd";
const FormItem = Form.Item;
export class Branch extends React.Component<any, any> {
    constructor(props: any) {
        super(props);
        const value = this.props.value || {}
        this.state = {
            issue: value.issue || "",
            branch: value.branch || ""
        }
    }
    componentWillReceiveProps(nextProps: any) {
        if ("value" in nextProps) {
            const value = nextProps.value;
            this.setState(value);
        }
    }

    handlerChange = (e, fieldName) => {
        const fieldValue = {
            [fieldName]: e.target.value || ""
        }
        if (!('value' in this.props)) {
            this.setState({ ...fieldValue });
        }
        this.triggerChange({ ...fieldValue })
    }

    triggerChange = (changeValue) => {
        const onChange = this.props.onChange;
        if (onChange) {
            onChange({ ...this.state, ...changeValue });
        }
    }
    render() {
        const { size } = this.props;
        const state = this.state;
        const formItemLayout = {
            labelCol: {
                span: 0,
                xs: { span: 24 },
                sm: { span: 6 },
            },
            wrapperCol: {
                span: 0,
                xs: { span: 24 },
                sm: { span: 14 },
            },
        };
        return (
            <span>
                <FormItem label="branch" {...formItemLayout}>
                    <Row gutter={8}>
                        <Col span={12}>
                            <FormItem label="" {...formItemLayout}>
                                <Input type="text"
                                    size={size}
                                    placeholder="issue number"
                                    value={state.issue}
                                    onChange={(e) => {
                                        this.handlerChange(e, "issue")
                                    }} />
                            </FormItem>
                        </Col>
                        <Col span={12}>
                            <FormItem label="From" {...formItemLayout}>
                                <Input type="text"
                                    size={size}
                                    value={state.branch}
                                    onChange={(e) => {
                                        this.handlerChange(e, "branch")
                                    }} />
                            </FormItem>
                        </Col>
                    </Row>
                </FormItem>
            </span>

        )
    }
}


