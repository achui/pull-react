import * as React from "react"
import { Form, Input, Button, Checkbox } from "antd";
const FormItem = Form.Item;
export class User extends React.Component<any, any> {
    constructor(props: any) {
        super(props);
        const value = this.props.value || {}
        this.state = {
            username: value.username || "",
            pwd: value.pwd || ""
        }
    }
    componentWillReceiveProps(nextProps: any) {
        if ("value" in nextProps) {
            const value = nextProps.value;
            this.setState(value);
        }
    }

    handlerChange = (e,fieldName) => {
        const username = e.target.value || "";
        const pppp = {
            [fieldName] : e.target.value
        }
        if (!('value' in this.props)) {
            this.setState({ ...pppp });
        }
        this.triggerChange({ ...pppp })
    }

    handlerChangePwd = (e) => {
        const pwd = e.target.value || "";
        if (!('value' in this.props)) {
            this.setState({ pwd });
        }
        this.triggerChange({ pwd })
    }

    triggerChange = (changeValue) => {
        const onChange = this.props.onChange;
        if (onChange) {
            onChange({ ...this.state, ...changeValue });
        }
    }
    render() {
        const { size } = this.props;
        const state = this.state;
        const formItemLayout = {
            labelCol: {
                span: 0,
                xs: { span: 24 },
                sm: { span: 6 },
            },
            wrapperCol: {
                span: 0,
                xs: { span: 24 },
                sm: { span: 14 },
            },
        };
        return (
            <span>
                <FormItem label="UserName" {...formItemLayout}>
                    <Input type="text"
                        size={size}
                        value={state.username}
                        onChange={(e) => {this.handlerChange(e, "username")
                        }} />
                </FormItem>
                <FormItem label="Password" {...formItemLayout}>
                    <Input type="text"
                        size={size}
                        value={state.pwd}
                         onChange={(e) => {this.handlerChange(e, "pwd")
                        }} />
                </FormItem>
                <FormItem label="Remember Me" {...formItemLayout}>
                    <Checkbox onChange={(e:any) => {
                       console.log("checked:" + e.target.checked)
                    }}></Checkbox>
                </FormItem>
            </span>
        )
    }
}


