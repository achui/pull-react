import * as React from "react"
import { Form, Input, Button, Checkbox, Row, Col } from "antd";
import {getDisplayCompsByCategory, getReleaseCompsByCheckValues} from "../config/ReleaseComp"
const FormItem = Form.Item;
const plainOptions = getDisplayCompsByCategory("BO");
export class FeatureBranch extends React.Component<any, any> {
    constructor(props: any) {
        super(props);
        const value = this.props.value || {}
        this.state = {
            branch: value.branch || [],
            releaseComp:getReleaseCompsByCheckValues(['fast-pi'])
        }
    }
    onChange = (checkValues) => {
        let releaseComp = getReleaseCompsByCheckValues(checkValues);
        console.log('checked = ', releaseComp);
        this.setState({
            branch: checkValues,
            releaseComp
        });
        this.triggerChange({
            branch: checkValues,
            releaseComp
        });
    }

    triggerChange = (checkValues) => {
        const onChange = this.props.onChange;
        const {branch, releaseComp} = checkValues;
        if (onChange) {
            onChange({ ...this.state, branch,  releaseComp});
        }
    }

    render() {
        const state = this.state;
        const formItemLayout = {
            labelCol: {
                span: 0,
                xs: { span: 24 },
                sm: { span: 6 },
            },
            wrapperCol: {
                span: 0,
                xs: { span: 24 },
                sm: { span: 14 },
            },
        };
        return (
            <span>
                <FormItem label="Feature Branch" {...formItemLayout}>
                    <Checkbox.Group options={plainOptions} onChange={this.onChange} value={this.state.branch}>
                    </Checkbox.Group>
                </FormItem>
            </span>
        )
    }
}


