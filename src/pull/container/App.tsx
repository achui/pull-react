import * as React from 'react';
import { connect } from "react-redux";
import { WrappedDemo } from "./Demo"
import { ajaxBatchCall } from "../actions/AjaxAction"


const mapDispatchToProps = (dispatch: any) => ({
    createBranch: (requests) => {
        dispatch(ajaxBatchCall(requests))
    }
})

const mapStateToProps = (state: any, ownProps: any) => ({
})

export const App = connect(mapStateToProps, mapDispatchToProps)(WrappedDemo);
