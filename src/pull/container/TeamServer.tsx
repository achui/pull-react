import * as React from "react"
import { Form, Input, Button, Checkbox, Row, Col } from "antd";
import {getDispayTeamServer, getTeamServerByCheckValue} from "../config/TeamServer"
const FormItem = Form.Item;
const plainOptions = getDispayTeamServer();
export class TeamServer extends React.Component<any, any> {
    constructor(props: any) {
        super(props);
        const value = this.props.value || {}
        this.state = {
            teamServer: value.teamServer || [],
        }
    }
    onChange = (checkValues) => {
        let ts = getTeamServerByCheckValue(checkValues);
        console.log('checked = ', checkValues);
        this.setState({
            teamServer: checkValues,
            checkedTS:ts
        });
        this.triggerChange({
            teamServer: checkValues,
            checkedTS:ts
        });
    }

    triggerChange = (checkValues) => {
        const onChange = this.props.onChange;
        const {teamServer, checkedTS} = checkValues;
        if (onChange) {
            onChange({ ...this.state, teamServer, checkedTS });
        }
    }

    render() {
        const state = this.state;
        const formItemLayout = {
            labelCol: {
                span: 0,
                xs: { span: 24 },
                sm: { span: 6 },
            },
            wrapperCol: {
                span: 0,
                xs: { span: 24 },
                sm: { span: 14 },
            },
        };
        return (
            <span>
                <FormItem label="Team Server" {...formItemLayout}>
                    <Checkbox.Group options={plainOptions} onChange={this.onChange} value={this.state.teamServer}>
                    </Checkbox.Group>
                </FormItem>
            </span>
        )
    }
}


