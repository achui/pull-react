import * as React from "react"
import * as ReactDOM from "react-dom";
import { Form, Button, Row, Col } from "antd";
import { User } from "./User"
import { Branch } from "./Branch"
import { FeatureBranch } from "./FeatureBranch"
import { TeamServer } from "./TeamServer"
import { RequestUtil } from "../util/RequestUtil"
import  cFetch  from "../service/baseAjaxService"
import {getDisplayCompsByCategory, getReleaseCompsByCheckValues} from "../config/ReleaseComp"
const FormItem = Form.Item;
class Demo extends React.Component<any, any> {
    handleSubmit = (e) => {
        e.preventDefault();
        this.props.form.validateFields((err, values) => {
            if (!err) {
                console.log('Received values of form: ', values);
            }
        });
    }
    createBranch = (e) => {
        e.preventDefault();
        console.log('props:', this.props);
        const createBranchEvent = this.props.createBranch;
        this.props.form.validateFields((err, values) => {
            if (!err) {
                console.log('Received values of form: ', values);
                const featureBranch:Array<any> = values.featureBranch.releaseComp;
                const {branch, issue} = values.branch;
                const {username, pwd} = values.user;
                let requests = [];
                featureBranch.forEach((item:any) => {
                    let request = RequestUtil.buildCreateBranchRequest(item, username, pwd, branch, issue);
                    requests.push(request);
                })

                console.log("request:",requests);
                createBranchEvent(requests);
            }
        });
    }
    render() {
        const { getFieldDecorator } = this.props.form;
        return (
            <Form>
                {getFieldDecorator('user', {
                    initialValue: { username: "portz", pwd: 'Achui_19800724' }
                })(<User />)}
                {getFieldDecorator('branch', {
                    initialValue: { issue: "", branch: 'develop' }
                })(<Branch />)}
                {getFieldDecorator('featureBranch', {
                    initialValue: { branch: ["fast-pi"],releaseComp:getReleaseCompsByCheckValues(['fast-pi']) }
                })(<FeatureBranch />)}
                {getFieldDecorator('teamServer', {
                    initialValue: { teamServer: ["BOV1-BOSEV1", "BOV2-Backend", "Frontend"] }
                })(<TeamServer />)}
                <Row gutter={8}>
                    <Col span={4}>
                        <Button type="primary" onClick={this.createBranch}>Create Branch</Button>
                    </Col>
                    <Col span={4}>
                        <Button type="primary">ConfigFile</Button>
                    </Col>
                    <Col span={4}>
                        <Button type="primary">CreateTS</Button>
                    </Col>
                    <Col span={4}>
                        <Button type="primary">RemoveTS</Button>
                    </Col>
                    <Col span={4}>
                        <Button type="primary">Delete Jenkins Jobs</Button>
                    </Col>
                </Row>
            </Form>
        );
    }
}

export const WrappedDemo = Form.create()(Demo);

//ReactDOM.render(<WrappedDemo />, document.getElementById('example'));
