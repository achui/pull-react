import { combineReducers } from 'redux'
import {
  SELECT_SUBREDDIT, INVALIDATE_SUBREDDIT,
  REQUEST_POSTS, RECEIVE_POSTS
} from '../actions/NewActions'

function selectedSubreddit(state = 'reactjs', action:any) {
  switch (action.type) {
  case SELECT_SUBREDDIT:
    return action.subreddit
  default:
    return state
  }
}

function posts(state = {
  isFetching: false,
  didInvalidate: false,
  items: new Array<any>()
}, action:any) {
  switch (action.type) {
    case INVALIDATE_SUBREDDIT:
      return {...state, didInvalidate:true}
    case REQUEST_POSTS:
     return {...state, isFetching: true, didInvalidate:true}
    case RECEIVE_POSTS:
      return {...state, isFetching: true, didInvalidate:true, items: action.posts, lastUpdated: action.receivedAt}
    default:
      return state
  }
}

function postsBySubreddit(state = { }, action:any) {
  switch (action.type) {
    case INVALIDATE_SUBREDDIT:
    case RECEIVE_POSTS:
    case REQUEST_POSTS:
      return {...state,
        [action.subreddit]: posts(state[action.subreddit], action)
      }
    default:
      return state
  }
}

const rootReducer = combineReducers({
  postsBySubreddit,
  selectedSubreddit
})

export default rootReducer
