import { Action } from '../actions/action'
import { combineReducers } from 'redux';

export namespace Store {

  export type Counter = { value: number }

  export type All = {
    counter: Counter
  }
}

const initialState: Store.Counter = {
    value: 0
}

function counter(state: Store.Counter = initialState, action: Action): Store.Counter {
    const { value } = state;
    switch (action.type) {
        case 'INCREMENT_COUNTER':
            const newValue = value + action.delta
            return { value: newValue }
        case 'RESET_COUNTER':
            return { value: 0 }
    }

    return state;
}

const reducers = combineReducers<Store.All>({
  counter: counter
})

export default reducers;

