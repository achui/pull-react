import * as fetch from 'isomorphic-fetch';

function check404(res) {
    if(res.status === 404) {
        return Promise.reject({message:"Error:404"});
    }
    return res;
}
function checkStatus(res) {
    if(res.status >=200 && res.status < 300) {
        return res;
    } else {
        return Promise.reject(`Error:{res.status}`);
    }
}

function jsonParse(res) {
    return res.json();
}
function cFetch(url, method = "GET", option) {
    const defaultOpts = {
        mode: 'cors',
    }
    const opts = { ...option };
    opts.headers = {
        ...option.headers,
        "Content-Type": "application/json"
    }

    return fetch(url, opts)
            .then(check404)
            .then(checkStatus)
            .then(jsonParse)
}

export default cFetch;
