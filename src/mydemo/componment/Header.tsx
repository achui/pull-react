import * as React from 'react';
import { connect } from "react-redux";
import { Link, Route } from 'react-router-dom'
import { withRouter } from 'react-router-dom'
import { About } from "../componment/About"
import { AjaxCtrl } from "../container/AjaxDemoContainer"
import { Counter } from "../container/Counter"
export class Header extends React.Component<any, any> {
    render() {
        return (
            <header>
                <nav>
                    <ul>
                        <li><Link to='/'>home</Link></li>
                        <li><Link to='/about'>about</Link></li>
                        <li><Link to='/about/1000'>about with Id</Link></li>
                        <li><Link to='/counter'>counter</Link></li>
                        <li><Link to='/ajax'>ajax</Link></li>
                    </ul>
                </nav>
            </header>

        );
    }
}
