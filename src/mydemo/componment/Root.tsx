import * as React from 'react';
import { Provider } from 'react-redux'
import { createStore, combineReducers, applyMiddleware } from "redux";
import reducers from "../reducers/CounterRecucer"
import ajaxReducer from "../reducers/AjaxReducer"
import { BrowserRouter, Route, Switch } from 'react-router-dom'
import { App } from "../componment/App"
import { About } from "../componment/About"
import { Counter } from "../container/Counter"
import { AjaxCtrl } from "../container/AjaxDemoContainer"
import api from "../middleware/api"
import { Router } from 'react-router'
import { routers } from "../routers/router"
import thunkMiddleware from 'redux-thunk'
import { createLogger } from 'redux-logger'
import { ConnectedRouter, routerReducer, routerMiddleware, push } from 'react-router-redux'
import createSagaMiddleware from 'redux-saga'
import { rootSaga } from '../../sagademo/saga/saga'
import createHistory from 'history/createHashHistory'
const history = createHistory()
const middleware = routerMiddleware(history)
const sagaMiddleware = createSagaMiddleware()
const loggerMiddleware = createLogger();
//const store = createStore(reducers);
const store = createStore(
    combineReducers({
        ...reducers,
        ...ajaxReducer,
        router: routerReducer
    }),
    applyMiddleware(middleware, sagaMiddleware, loggerMiddleware)
)
sagaMiddleware.run(rootSaga);
export class RootComp extends React.Component<any, any> {
    render() {
        return (
            <Provider store={store}>
                <ConnectedRouter history={history}>
                    <Route component={App} />
                </ConnectedRouter>
            </Provider>
        );
    }
}
