import * as React from 'react';

export class AboutDetail extends React.Component<any,any> {
    sleep(timeout) {
        return new Promise((resolve, reject) => {
            setTimeout(() => {
                resolve("ok")
            }, timeout);
        })
    }
    async clickBtn() {
        console.log('start');
        let result = await this.sleep(3000);
        console.log('end, result:' +result);
    }
    render() {
        let {match} = this.props;
        return (
            <div>
                About Detail id: {match.params.id}.
            </div>
        );
    }
}
