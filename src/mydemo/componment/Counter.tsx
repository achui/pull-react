import * as React from 'react';

export class CounterComponent extends React.Component<any, any> {

    render() {
        const {counter, label} = this.props;
        console.log("counter:"+counter.value);
        return (
            <div>
                <b>label: {label}</b>
                <p># Counter : {counter.value}</p>
                <button id="toggle-preview" onClick={e => {
                    console.log("===click"+this.props.increase)
                    this.props.increase(1)}
                }>+</button>
                <button onClick={e => this.props.increase(-1)}>+</button>
                <button onClick={e => this.props.increaseAsync(1)}>inrease  after 1 second</button>
                <button id="abc">+++</button>

            </div>

        );
    }
}
