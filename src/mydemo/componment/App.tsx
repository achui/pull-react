import * as React from 'react';
import { connect } from "react-redux";
import { Link, Route } from 'react-router-dom'
import { withRouter } from 'react-router-dom'
import { Header } from "../componment/Header"
import { Main } from "../componment/Main"
export class AppComp extends React.Component<any, any> {
    render() {
        return (
            <div>
                <Header />
                <Main />
            </div>

        );
    }
}

export const App = connect()(AppComp);
