import * as React from 'react';
import { BrowserRouter, Route, Switch } from 'react-router-dom'
import { About } from "../componment/About"
import { AboutDetail } from "../componment/AboutDetail"
import { Home } from "../componment/Home"
import { AjaxCtrl } from "../container/AjaxDemoContainer"
import { Counter } from "../container/Counter"
export class Main extends React.Component<any, any> {
    render() {
        return (
            <main>
                <Switch>
                    <Route exact={true} path='/' component={Home} />
                    <Route path='/about/:id' component={AboutDetail} />
                    <Route path='/about' component={About} />
                    <Route path='/counter' component={Counter} />
                    <Route path='/ajax' component={AjaxCtrl} />

                </Switch>
            </main>

        );
    }
}
