import * as React from 'react';
import { Link, Route } from 'react-router-dom'
export class About extends React.Component<any, any> {

    sleep(timeout) {
        return new Promise((resolve, reject) => {
            setTimeout(() => {
                resolve("ok")
            }, timeout);
        })
    }
    async clickBtn() {
        console.log('start');
        let result = await this.sleep(3000);
        console.log('end, result:' + result);
    }
    render() {
        let {match} = this.props
        console.log("Match:%O", match)
        return (
            <div>
                <div>
                    <Link to="/about/1000">Detail</Link>
                </div>
                <div>
                    Hello, React by Typescript.
                <br />
                    URL: {match.url}
                    <br />
                    {match.params.id && <div>URL Param id: {match.params.id}</div>}
                    <button onClick={e => this.clickBtn()}>btn</button>
                </div>
            </div>
        );
    }
}
