import * as React from 'react';

export class AjaxDemo extends React.Component<any, any> {
    componentDidMount() {
        this.props.loadData();
    }
    render() {
        let {isFecting} = this.props.ajaxRequest;
        return (
            <div>
                Hello Achui
                <p>Message:{this.props.ajaxRequest.error}</p>
                {!isFecting && this.props.ajaxRequest.data &&
                    <p>Length:{this.props.ajaxRequest.data.children.length}</p>
                }
                <p>Status:{this.props.ajaxRequest.isFecting}</p>
                {isFecting && <h2>Loading...</h2>}
            </div>

        );
    }
}

