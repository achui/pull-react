import { connect } from "react-redux";
import { CounterComponent } from "../componment/Counter"
import { increaseCounter, increaseCounterAsync } from "../actions/Action"
import { withRouter } from 'react-router-dom'
const mapStateToProps = (state: any, ownProps:any) => ({
    counter: state.counter
})

const mapDispatchToProps = (dispatch: any) => ({
    increase: (by: number) => {
        dispatch(increaseCounter(by))
    },
    increaseAsync: (by:number) => {
        dispatch(increaseCounterAsync())
    }
})

export const Counter = connect(mapStateToProps, mapDispatchToProps)(CounterComponent);
