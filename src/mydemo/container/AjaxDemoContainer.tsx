import { AjaxRequestSuccess } from "../actions/AjaxAction"
import { connect } from "react-redux";
import { AjaxDemo } from "../componment/AjaxDemo"
const mapStateToProps = (state: any, ownProps:any) => ({
    ajaxRequest: state.ajaxRequest
})

const mapDispatchToProps = (dispatch: any) => ({
    loadData:() => dispatch({type:"AJAX_ASYNC"})
})

export const AjaxCtrl = connect(mapStateToProps, mapDispatchToProps)(AjaxDemo);
