import { Action } from "../actions/Action";
import { combineReducers } from "redux";
function counter(state = { value: 0 }, action: Action) {
    let newValue: number;
    switch (action.type) {
        case 'INCREASE_COUNTER':
            newValue = state.value + action.playLoad;
            return { value: newValue }
        case 'DECREASE_COUNTER':
            newValue = state.value - action.playLoad;
            return { value: newValue }
        default:
            return state;
    }
}

const reducers = {
    counter
}

export default reducers;
