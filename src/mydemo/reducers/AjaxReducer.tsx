
function request(state={}, action) {
    switch(action.type) {
        case "AJAX_REQUEST_SUCCESS":
            return {...state, isFecting:false, data:action.response.data}
        case "AJAX_REQUEST_START":
            return {...state, isFecting:true}
        case "AJAX_REQUEST_FAIL":
            return {...state, error:action.error, isFecting:false}
        default:
            return state;
    }
}

const ajaxReducer = {
    ajaxRequest:request
}

export default ajaxReducer

