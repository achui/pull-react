import cFetch from '../service/baseAjaxService';

export const AjaxRequestStart = () => ({
    type:"AJAX_REQUEST_START",
})

export const AjaxRequestSuccess = () => ({
    ["Call API"]: cFetch("https://cors-anywhere.herokuapp.com/https://www.reddit.com/r/front.json","GET",{}),
    type:"AJAX_REQUEST_SUCCESS",
})

export const AjaxRequestFail = () => ({
    type:"AJAX_REQUEST_FAIL",
    error:"FAIL"
})
