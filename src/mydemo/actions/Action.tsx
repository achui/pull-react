export interface Action {
    type : string;
    playLoad? : any;
}

export const increaseCounter = (by: number): Action => ({
    type:'INCREASE_COUNTER',
    playLoad:by
})

export const increaseCounterAsync = (): Action => ({
    type:'INCREMENT_ASYNC'
})

export const AjaxRequestStart = () => ({
    type:"AJAX_REQUEST_START",
    isFetching:true
})

export const AjaxRequestSuccess = () => ({
    type:"AJAX_REQUEST_SUCCESS",
    isFetching:false
})

export const AjaxRequestFail = () => ({
    type:"AJAX_REQUEST_FAIL",
    isFetching:false,
    error:"FAIL"
})
