import * as React from "react"
import { Route, Switch } from 'react-router-dom'
import { App } from "../componment/App"
import { About } from "../componment/About"
import { AjaxCtrl } from "../container/AjaxDemoContainer"
import { Counter } from "../container/Counter"
export const routers = (
    <div>
        <Route exact path="/" component={App} />
        <Route path="/about" component={About} />
        <Route path="/ajax" component={AjaxCtrl} />
        <Route path="/ajaxok" component={AjaxCtrl} />
        <Route path="/counter" children={() => <Counter label="aaa5"/>} />
    </div>
);
