var webpack = require('webpack');
var HtmlWebpackPlugin = require('html-webpack-plugin');
module.exports = {
    entry: {
        app: "./src/pull/index.tsx"
    },
    output: {
        path: __dirname + "/dist",
        filename: '[name].js',
        chunkFilename: '[id].chunk.js'
    },

    // Enable sourcemaps for debugging webpack's output.
    devtool: "source-map",

    resolve: {
        // Add '.ts' and '.tsx' as resolvable extensions.
        extensions: [".ts", ".tsx", ".js", ".json"]
    },

    module: {
        rules: [
            // All files with a '.ts' or '.tsx' extension will be handled by 'awesome-typescript-loader'.
            /*{ test: /\.tsx?$/, loader: "babel-loader!awesome-typescript-loader" },*/
            { test: /\.tsx?$/, loader: "awesome-typescript-loader" },

            { test: /\.css?$/, loader: 'style-loader!css-loader' },
            { test: /\.less?$/, loader: 'css-loader!less-loader' },

            // All output '.js' files will have any sourcemaps re-processed by 'source-map-loader'.
            { enforce: "pre", test: /\.js$/, loader: "source-map-loader" }
        ]
    },

    devServer: {
        historyApiFallback: true,
        stats: 'minimal',
        proxy: {
            "/appi": {
                target: 'https://tcc.taobao.com',
                secure: false
            },
            "/rest": {
                target: 'https://git.ehealthinsurance.com',
                secure: false
            },
        }
    },

    // When importing a module whose path matches one of the following, just
    // assume a corresponding global variable exists and use that instead.
    // This is important because it allows us to avoid bundling all of our
    // dependencies, which allows browsers to cache those libraries between builds.
    externals: {
        "react": "React",
        "react-dom": "ReactDOM",
        "antd": "antd"
    },
    plugins: [
        new webpack.optimize.CommonsChunkPlugin({
            name: ['app']
        }),

        new HtmlWebpackPlugin({
            template: 'index.html'
        })
    ]
};